// Copyright (c) OpenRobotGroup 2022.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

import io.github.oblarg.oblog.Logger;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {

  // Robot Container
  private RobotContainer m_robotContainer;

  // Autonomous Command
  private Command m_autonomousCommand;

  /** Runs when the robot is first started up and should be used for any initialization code. */
  @Override
  public void robotInit() {
    // Instantiate the RobotContainer.
    m_robotContainer = new RobotContainer();

    // Disable live window telemetry, as it is only necessary in test mode, in which case the
    // disable is overriden.
    LiveWindow.disableAllTelemetry();

    // Configure the Oblog logger.
    Logger.configureLoggingAndConfig(m_robotContainer, false);
  }

  /**
   * Called every 20 ms, no matter the mode. Use this for items like diagnostics that you want ran
   * during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    // Runs the Scheduler.  This is responsible for polling buttons, adding newly-scheduled
    // commands, running already-scheduled commands, removing finished or interrupted commands,
    // and running subsystem periodic() methods.  This must be called from the robot's periodic
    // block in order for anything in the Command-based framework to work.
    CommandScheduler.getInstance().run();

    // Update the Oblog logger.
    Logger.updateEntries();
  }

  /** Called once each time the robot enters disabled mode. */
  @Override
  public void disabledInit() {}

  /** Called periodically during disabled mode. */
  @Override
  public void disabledPeriodic() {}

  /** Initializes the robot for teleoperated mode. */
  @Override
  public void autonomousInit() {
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    // Schedule the autonomous command.
    if (m_autonomousCommand != null) m_autonomousCommand.schedule();
  }

  /** Called periodically during autonomous mode. */
  @Override
  public void autonomousPeriodic() {}

  /** Initializes the robot for teleoperated mode. */
  @Override
  public void teleopInit() {
    // Ensures the autonomous command is canceled when transitioning to teleoperated mode.
    if (m_autonomousCommand != null) m_autonomousCommand.cancel();
  }

  /** Called periodically during teleoperated mode. */
  @Override
  public void teleopPeriodic() {}

  /** Initializes the robot for test mode. */
  @Override
  public void testInit() {
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /** Called periodically during test mode. */
  @Override
  public void testPeriodic() {}

  /** Initializes the robot for simulation. */
  @Override
  public void simulationInit() {}

  /** Called periodically whilst in simulation. */
  @Override
  public void simulationPeriodic() {}
}
